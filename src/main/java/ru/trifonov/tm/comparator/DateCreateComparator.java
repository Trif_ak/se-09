package ru.trifonov.tm.comparator;

import ru.trifonov.tm.entity.AbstractComparableEntity;

import java.util.Comparator;

public class
DateCreateComparator implements Comparator<AbstractComparableEntity> {
    @Override
    public int compare(AbstractComparableEntity o1, AbstractComparableEntity o2) {
        return o1.getCreateDate().compareTo(o2.getCreateDate());
    }
}
