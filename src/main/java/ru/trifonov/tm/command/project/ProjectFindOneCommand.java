package ru.trifonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ProjectFindOneCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-findOne";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT FIND]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter the ID of the project");
        @Nullable final String id = serviceLocator.getInCommand().nextLine();
        System.out.println(serviceLocator.getProjectService().findOne(id, userId));
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
