package ru.trifonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserRemoveOneCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-removeOne";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": removeOne select user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REMOVE ONE]");
        if (serviceLocator.getCurrentUser() == null) throw new IllegalArgumentException("User is not authorized");
        System.out.println("Enter the ID of the user you want to removeOne");
        final String id = serviceLocator.getInCommand().nextLine();
        serviceLocator.getUserService().removeOne(id);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};

    }
}
