package ru.trifonov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;

@UtilityClass
public class HashUtil {
    public static String getHashMD5(String password) throws Exception{
        @NotNull final byte[] bytesPassword = password.getBytes("UTF-8");
        return new String(MessageDigest.getInstance("MD5").digest(bytesPassword));
    }
}
