package ru.trifonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class TaskRemoveAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-removeAllOfProject";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": removeOne select tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE ALL]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter ID of project");
        @Nullable final String projectId = serviceLocator.getInCommand().nextLine();
        serviceLocator.getTaskService().removeAllOfProject(projectId, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
