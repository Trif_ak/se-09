package ru.trifonov.tm.comparator;

import ru.trifonov.tm.entity.AbstractComparableEntity;

import java.util.Comparator;

public class DateEndComparator implements Comparator<AbstractComparableEntity> {
    @Override
    public int compare(AbstractComparableEntity o1, AbstractComparableEntity o2) {
        return o1.getBeginDate().compareTo(o2.getBeginDate());
    }
}
