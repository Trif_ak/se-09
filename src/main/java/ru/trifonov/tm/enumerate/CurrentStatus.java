package ru.trifonov.tm.enumerate;

public enum CurrentStatus {
    PLANNED("planned"), PROCESS("in process"), COMPLETED("completed");
    private String status;

    CurrentStatus(String status) {
        this.status = status;
    }

    public String getCurrentStatus() {
        return status;
    }
}
