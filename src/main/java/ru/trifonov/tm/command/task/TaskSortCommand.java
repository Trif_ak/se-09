package ru.trifonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.List;

public class TaskSortCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "task-sort";
    }

    @Override
    public @NotNull String getDescription() {
        return ": sort your tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SORT]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter ID of project");
        @Nullable final String projectId = serviceLocator.getInCommand().nextLine();
        System.out.println("Select the sort type: \n date-create \n date-begin \n date-end \n status");
        @Nullable final String comparatorName = serviceLocator.getInCommand().nextLine();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().sortBy(projectId, userId, comparatorName);
        for (@NotNull final Task task : tasks) {
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
