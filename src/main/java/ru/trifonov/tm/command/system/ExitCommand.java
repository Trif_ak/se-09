package ru.trifonov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ExitCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": program exit";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROGRAM EXIT]");
        System.exit(0);
    }

    @Override
    public RoleType[] roleType() {
        return null;
    }
}
