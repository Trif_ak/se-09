package ru.trifonov.tm;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.bootstrap.*;

import java.util.Set;


public final class Application {
    private static final Set<Class<? extends AbstractCommand>> CLASSES = new Reflections("ru.trifonov.tm.command").getSubTypesOf(AbstractCommand.class);

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }
}