package ru.trifonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;
import java.util.List;

public final class UserFindAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-findAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return all users";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USERS FIND ALL]");
        if (serviceLocator.getCurrentUser() == null) throw new IllegalArgumentException("User is not authorized");
        final Collection<User> inputList = serviceLocator.getUserService().findAll();
        for (final User user : inputList) {
            System.out.println(user);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};

    }
}
